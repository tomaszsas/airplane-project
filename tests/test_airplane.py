from assertpy import assert_that
from src.airplane import Airplane


def test_new_airplane_has_milage_0():
    airplane = Airplane("Boeing-727", 500)
    assert (assert_that(airplane.milage).is_equal_to(0)
            and assert_that(airplane.occupied_seats).is_equal_to(0))

def test_airplane_after_course_5000_has_5000_milage():
    airplane = Airplane("Boeing-727", 500)
    airplane.fly(5000)
    assert assert_that(airplane.milage).is_equal_to(5000)

def test_airplane_has_correct_milage():
    airplane = Airplane("Boeing-727", 500)
    airplane.fly(5000)
    airplane.fly(3000)
    assert  assert_that(airplane.milage).is_equal_to(8000)

def test_airplane_after_course_9999_not_required_service():
    airplane = Airplane("Boeing-727", 500)
    airplane.fly(9999)
    assert assert_that(airplane.is_service_required()).is_equal_to(False)


def test_airplane_after_course_10001_required_service():
    airplane = Airplane("Boeing-727", 500)
    airplane.fly(10001)
    assert assert_that(airplane.is_service_required()).is_equal_to(True)
