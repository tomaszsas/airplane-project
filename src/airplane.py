class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.milage = 0
        self.number_of_seats = number_of_seats
        self.occupied_seats = 0

    def fly(self, distance):
        self.milage += distance

    def is_service_required(self):
        if self.milage > 10_000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if self.get_available_seats() - number_of_passengers < 0:
            raise ValueError("Plane doesn't have enough seats")
        else:
            self.occupied_seats += number_of_passengers

    def get_available_seats(self):
        return self.number_of_seats - self.occupied_seats


if __name__ == '__main__':
    plane_1 = Airplane("Boeing-727", 500)
    plane_2 = Airplane("Airbus-389", 300)

    plane_1.fly(10_000)
    plane_1.fly(2_000)
    plane_2.fly(5_000)

    print("Is plane 1 required a service:", plane_1.is_service_required())
    print("Is plane 2 required a service:", plane_2.is_service_required())

    plane_1.board_passengers(450)
    plane_2.board_passengers(250)

    print("Available seats for plane 1:", plane_1.get_available_seats())
    print("Available seats for plane 2:", plane_2.get_available_seats())

    #  Raising ValueError
    plane_1.board_passengers(50)
    print("Available seats for plane 1:", plane_1.get_available_seats())
